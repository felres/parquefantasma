/// wall "collisions"
unoccupied_angle = navangle;
var extra_force = 1;
for(var i = 0; i < 360; i++)
{
    var s = 1; if is_even(i) s = -1;
    var ang = navangle + i*s;
    var x2 = x+lengthdir_x(sense, ang);
    var y2 = y+lengthdir_y(sense, ang);
    var wall = collision_line(x, y, x2, y2, objCaster, true, true);
    if !wall
    {
        unoccupied_angle = ang;
        break;
    }
    else
        extra_force = max(1, abs(wall.speed));
}
var wall_turnrate = extra_force*turnspd;
navangle = angle_approach(navangle, unoccupied_angle, wall_turnrate);
