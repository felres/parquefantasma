/// scrCopDetected
if state_new
{
    audio_play_sound_robot(choose(sndCopAlertaHumano1, sndCopAlertaHumano2, sndCopAlertaHerramineta1, sndCopAlertaHerramineta2));
    with(myLight)light_set_color(other.col_danger);
}

cop_steer_avoid_walls();
navangle = angle_normalize(point_direction(x, y, player_instance.x, player_instance.y));
// Movement
spd = maxSpd*0.3;
horSpd = lengthdir_x(spd, navangle);
verSpd = lengthdir_y(spd, navangle);
x += horSpd;
y += verSpd;

if( state_timer >= detectTime )
{
    state_switch("rush");
}
