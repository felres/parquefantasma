/// scrCopSearching
if state_new
{
    
}

var _time = 360/scan_turnspd;
var spins = 3;
navangle += scan_turnspd*spins;

/// player shoot
var col = collision_fov(x, y, objPlayer, sense, navangle, fov_angle, true);
if col && !collision_line(x, y, col.x, col.y, objCaster, true, true)
{
    player_instance = col;
    audio_stop_sound_force(current_sound);
    state_switch("shoot");
}

if( state_timer == 20 )
    current_sound = audio_play_sound_robot(choose(sndCopDesaparecido1, sndCopDesaparecido2, sndCopDesaparecido3, sndCopDesaparecido4, sndCopDesaparecido1, sndCopDesaparecido2, sndCopDesaparecido3, sndCopDesaparecido4, sndCopDesaparecidoPicha));
if( state_timer >= _time )
{
    state_switch("return");
}
