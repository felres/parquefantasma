/// scrCopShoot
if state_new
{
    //audio_stop_sound_force(current_sound);
    current_sound = audio_play_sound_robot(choose(sndCopGun1, sndCopGun2, sndCopGun1, sndCopGun2, sndCopGunPraPraPra));
    navangle = point_direction(x, y, player_instance.x, player_instance.y);
}

if audio_is_playing(current_sound)
{
    if(random(1.9) < 1)
    {   
        var s = audio_play_sound(sndShoot, -1, false);
        audio_sound_pitch(s,gauss(1, 0.2));
        audio_sound_gain(s,0.5, 0);
        with instance_create(x, y, objProyectile)
        {
            direction = gauss(other.navangle, 20);
            image_angle = direction;
            speed = 10;
        }
    }
}
else
{
    state_switch("searching");
}
