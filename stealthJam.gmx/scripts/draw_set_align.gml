/// draw_set_align([fa_left, fa_center, fa_right], [fa_top, fa_middle, fa_bottom])

var halign = fa_left;
if (argument_count>0) halign = argument[0];
var valign = fa_top;
if (argument_count>1) valign = argument[1];

draw_set_halign(halign);
draw_set_valign(valign);
