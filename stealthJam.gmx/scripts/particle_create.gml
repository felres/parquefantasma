/// particle_create(x, y, sprite_index, image_scale, image_speed, alpha_speed, depth, destroy_at_anim_end?, destroy_at_alpha_more_than_1?, destroy_at_alpha_less_than_0?)

particle_create_ext(argument[0],
                    argument[1],
                    argument[2],
                    0,
                    argument[3],
                    0,
                    c_white,
                    1,
                    argument[4],
                    0,
                    0,
                    0,
                    0,
                    c_white,
                    0,
                    argument[5],
                    argument[6],
                    argument[7],
                    argument[8],
                    argument[9]
                    );
