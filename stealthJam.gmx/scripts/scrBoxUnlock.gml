/// scrBoxUnlock

if state_new
{
    image_speed = 30/60;
    audio_play_sound(sndBoxOpen, 2, false);
}

// stop animating on finish
if image_index == 2
{
    image_index = 2;
    image_speed = 0;
}

// anim
var anim_t = 30;
image_xscale = 1 + xscale_animextra - EaseOutElastic(state_timer, 0, xscale_animextra, anim_t);

if state_timer >= anim_t
    state_switch("open"); 
