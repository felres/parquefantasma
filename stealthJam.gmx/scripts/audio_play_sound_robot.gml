///audio_play_sound_robot(id)

var sndId = argument0;
var rad = 250;
var setgain = 2;
if instance_exists(objPlayer)
    setgain *= 1-clamp(distance_to_object(objPlayer)/rad, 0, 1);
with instance_create(x, y, objSoundDelayed)
{
    soundid = sndId;
    pitch = 0.93;
    gain = setgain;
    event_perform(ev_alarm, 0);
}
with instance_create(x, y, objSoundDelayed)
{
    soundid = sndId;
    pitch = 0.95;
    gain = setgain;
    alarm[0] = 1;
}
with instance_create(x, y, objSoundDelayed)
{
    soundid = sndId;
    pitch = 1.002;
    gain = setgain;
    alarm[0] = 2;
}
with instance_create(x, y, objSoundDelayed)
{
    soundid = sndId;
    pitch = 0.97;
    gain = setgain;
    alarm[0] = 3;
}
return sndId;

