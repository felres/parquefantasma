#define _state_template
//So, here's your basic state template

//STEP---------------------------------------
if(argument0==ts_step)
{
//This code will be executed during the step event.
}
//DRAW---------------------------------------
else if(argument0==ts_draw)
{
//And this code will be exeucted during the draw event.
}


#define truestate_cleanup
///truestate_destroy()
/// Put in the cleanup event of the object.
/// Cleans up all related data structures.

ds_map_destroy(state_map);
ds_map_destroy(state_names);
ds_stack_destroy(state_stack);
ds_queue_destroy(state_queue);


#define truestate_clear_history
///truestate_clear_history()
/// Empties the previous state stack to prevent getting to big.
/// Recommended you call it when you are at a "default" state.

ds_stack_clear(state_stack);
ds_stack_push(state_stack,state);


#define truestate_create
/// @description truestate_create(StateEnum,Script,Name<Optional>)
ds_map_replace(state_map,argument[0],argument[1]);
if(argument_count > 2)
  ds_map_replace(state_names,argument[0],argument[2]);
else
  ds_map_replace(state_names,argument[0],script_get_name(argument[1]));

/*Your state scripts should be broken up into 2 parts like this:
#region Step
if(argument[0])
{
//step
}

#endregion

#region Draw
else
{
//draw
}
#endregion

The top part will be called during the step event, the bottom part will be called during the draw event.



#define truestate_draw
///truestate_draw()
///Call this in the draw event of your object.

if(script_exists(state_script))
  script_execute(state_script,draw)
else
  truestate_switch(state_default);


#define truestate_draw_current
///truestate_draw_current(x,y)
/// Useful debug script that draws the current state name to the screen 
/// as well as the current state timer value.

var _str = state_names[? state] + " ("+string(state_timer)+")";
draw_text(argument0,argument1,_str);


#define truestate_draw_gui_end
///truestate_draw_gui_end()

/// Script that executes after all other logic has been performed for the object.
/// Will perform the ACTUAL state switching, and also resets timers.
/// Where you put this can vary.  I recommend draw gui end, which is the last event any object runs.
/// You could put it in the pre draw or end step event if you want states to switch before the draw 
/// event occurs (not recommended), but that is possible.

state_switch_locked=false; //Release the lock

if(state_next != state)
{ //Switch to the new state
state_previous=state;
if(state_next == ts_queue)
{
 state_next=ds_queue_dequeue(state_queue);
 ds_stack_push(state_stack,state_next);
}

  state=state_next;
  state_script=state_map[? state];
  state_timer=0;
  state_new=true;
}
else
{ //Increment current state timer
  state_timer++;
  state_new=false;
}

return state_new;




#define truestate_enqueue
///truestate_enqueue(State1,State2,State3,...)
/// Adds any number of states to the queue.


var _i=0;
repeat(argument_count)
{
  var _state = argument[_i];
  if(truestate_exists(_state))
    ds_queue_enqueue(state_queue,_state);
  else
    show_debug_message("Tried to queue a non-existent state");
  _i++;
}


#define truestate_exists
///truestate_exists(StateEnum)
/// Returns true if the state exists for the calling actor.

return(ds_map_exists(state_map,argument[0]));

/*This is useful in cases where you are using a general state that many actors use, but some actors have
additional states.

For example, pretty much every actor object might have "States.stand" but only playable characters would
have a "state_idle_animation" when they stand around too long.  So you wouldn't want to wait 3 seconds
and then move to the idle state if the actor doesn't have one, so use this to check first.


#define truestate_get_name
/// truestate_get_name(StateEnum)
/// Returns the string name of the passed state.

var _name = state_names[? argument0];
if(_name == undefined)
  return "Undefined";
return _name;


#define truestate_queue_start
///truestate_queue_start()

/// Begins the state queue.
state_in_queue = ds_queue_size(state_queue) > 0;
truestate_switch();



#define truestate_set_default
///truestate_set_default(StateEnum)

/// Sets the default/first state for the object.  Called only in the create event, typically after you've defined
/// all the states for this object.

state=argument0;
state_script=ds_map_find_value(state_map,argument0);    

state_default = state;

state_next=state;
ds_stack_push(state_stack,state);
state_new=true;


#define truestate_step
///truestate_step()

/// Call this in the step event of your object.
if(script_exists(state_script))
  script_execute(state_script,step)
else
  truestate_switch(state_default);


#define truestate_switch
///truestate_switch(StateEnum,lock<optional>)/// Switches to a new state at the end of this step.
/// If you lock the state switch, any other state switches will be ignored until this change happens 
/// the following step.
/// Finally, if you are in the middle of executing a state queue, any state switch will be
/// interpreted as a "go to next".  You can call this script with no arguments in that case, or to return to the default state.

//Queue handling
if(state_switch_locked) 
{
  if(state_in_queue)
  {//The locked state will interrupt the queue
    ds_queue_clear(state_queue);
    state_in_queue=false;
  }
  exit;
}
if(state_in_queue && ds_queue_size(state_queue)>0)
{
  state_next = ts_queue;
  exit;
}
state_in_queue = false;

//Switch to default
var _lock = false;
if(argument_count == 0)
{
  state_next = state_default;
  exit;
}

if(ds_map_exists(state_map,argument[0]))
{
  state_next=argument[0];
}
else
{
  show_debug_message("Tried to switch to a non-existent state("+string(argument[0])+").  Moving to default state.")
  state_next=state_default;
}

//Push to stack if not locked.
if(!state_stack_locked && ds_stack_top(state_stack) != state_next) 
  ds_stack_push(state_stack,state_next);
else
  state_stack_locked=false; //Reset the lock on the stack.

if(argument_count>1)
  state_switch_locked=argument[1];


#define truestate_switch_previous
///truestate_switch_previous()
///Returns to the previous state.
if(state_in_queue)
{
  truestate_switch();
  exit;
}
if(ds_stack_empty(state_stack))
{
  truestate_switch(state_default);
  exit;
}

ds_stack_pop(state_stack);
state_stack_locked=true;
truestate_switch(ds_stack_top(state_stack));


#define truestate_system_init
///truestate_system_init()

/// Initilize the variables required for the state engine.
/// Call this on any object you want to use the state machine in the create event.

///Recommend moving these to macros once imported to your project.
globalvar ts_step,ts_draw,ts_queue;
ts_step = 1;
ts_draw = 0;
ts_queue = 9999;

state=noone;
state_script=noone
state_next=state;
state_previous=state;
state_switch_locked=false;
state_stack_locked=false;
state_default=noone;

state_timer=0;

state_new=true;
state_var[0]=0; //Useful for storing variables specific to a specific state.

state_map=ds_map_create();
state_names=ds_map_create(); 
state_stack=ds_stack_create();
state_queue=ds_queue_create();
state_in_queue=false;
