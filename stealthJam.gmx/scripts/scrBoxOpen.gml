if state_new
{
    var spd = 8;
    var aspd = -0.03;
    particle_create_ext(x, y + yextra, sprToolboxTop, 0, extra_scale,
                        0, c_white, 1, 0, 90, spd, 0, 0, c_white, 0,
                        aspd, depth, false, false, true);
    particle_create_ext(x, y + yextra, sprToolboxBottom, 0, extra_scale,
                        0, c_white, 1, 0, 270, spd, 0, 0, c_white, 0,
                        aspd, depth, false, false, true);
}


instance_destroy();
