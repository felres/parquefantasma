/// speech_say(xoffset, yoffset, str)
var insid, xoff, yoff, str, ins;
insid = id;
xoff = argument0;
yoff = argument1;
str = argument2;

ins = instance_create(x, y, objBubble)
with ins
{
    follow = insid;
    xoffset = xoff;
    yoffset = yoff;
    text = str;
}
return ins;
