/// scrCopPause
if(state_new)
{
    if (random(5) < 1)
        audio_play_sound_robot(choose(sndCopPatrullando1, sndCopPatrullando2, sndCopPatrullando3));
}

/// player detection
var col = collision_fov(x, y, objPlayer, sense, navangle, fov_angle, true);
if col && !collision_line(x, y, col.x, col.y, objCaster, true, true)
{
    player_instance = col;
    state_switch("detected");
}

if state_timer >= pauseTime
{
    state_switch("looking");
}
