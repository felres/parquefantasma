/// scrCopReturn
if state_new 
{
    audio_play_sound_robot(choose(sndCopVolverAPatrullar1, sndCopVolverAPatrullar2, sndCopVolverAPatrullar3));
    with(myLight)light_set_color(other.col_normal);
}


navangle = point_direction(x, y, xstart, ystart);
cop_steer_avoid_walls();
// Movement
spd = maxSpd;
horSpd = lengthdir_x(spd, navangle);
verSpd = lengthdir_y(spd, navangle);
x += horSpd;
y += verSpd;

/// player detection
var col = collision_fov(x, y, objPlayer, sense, navangle, fov_angle, true);
if col && !collision_line(x, y, col.x, col.y, objCaster, true, true)
{
    player_instance = col;
    state_switch("detected");
}

if( point_distance(x, y, xstart, ystart) <= maxSpd*2 )
{
    navangle = 0;
    last_redirector = noone;
    player_instance = noone;
    x = xstart;
    y = ystart;
    current_sound = audio_play_sound_robot(choose(sndCopPatrullando1, sndCopPatrullando2, sndCopPatrullando3));
    state_switch("walking");
}

