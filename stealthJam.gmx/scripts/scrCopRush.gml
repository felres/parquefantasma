/// scrCopRush
if state_new
{
    current_sound = audio_play_sound_robot(choose(sndCopCaptura1, sndCopCaptura2, sndCopCaptura3, sndCopCaptura4));
}

var targetang = angle_normalize(point_direction(x, y, player_instance.x, player_instance.y));
navangle = angle_approach(navangle, targetang, turnspd);
cop_steer_avoid_walls();
// Movement
spd = maxSpd*2.5;
horSpd = lengthdir_x(spd, navangle);
verSpd = lengthdir_y(spd, navangle);
x += horSpd;
y += verSpd;

/// particles
if(random(3) < 1)
{   
    particle_create_ext(x, y,
                        sprSmoke, 0, 0.7, 0, c_ltgray, 1, 1,
                        gauss(navangle+180, 8), 5, 0, 20,
                        c_white, 0, -0.01, depth+1, false, false, true);
}

if( state_timer >= rushTime && !audio_is_playing(current_sound) )
{
    state_switch("searching");
}
