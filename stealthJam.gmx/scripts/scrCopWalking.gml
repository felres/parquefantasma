/// scrCopWalking
if(state_new)
{
    if !audio_is_playing(current_sound)
        audio_play_sound_robot(choose(sndCopBeepBeep, sndCopBeep1, sndCopBeep2, sndCopBeep3));
    with(myLight)light_set_color(other.col_normal);
}

// Movement
spd = maxSpd;
horSpd = lengthdir_x(spd, navangle);
verSpd = lengthdir_y(spd, navangle);
x += horSpd;
y += verSpd;

/// Check redirector arrows collision
var redirector = instance_place(x + horSpd, y + verSpd, objCopRedirector);
if( redirector && (redirector != last_redirector) )
{    
    // ir a proximo estado
    last_redirector = redirector;
    state_switch("pause");
}

/// player detection
var col = collision_fov(x, y, objPlayer, sense, navangle, fov_angle, true);
if col && !collision_line(x, y, col.x, col.y, objCaster, true, true)
{
    player_instance = col;
    state_switch("detected");
}
