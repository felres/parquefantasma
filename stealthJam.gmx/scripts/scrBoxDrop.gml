/// scrBoxDrop
var anim_t = 30;

yextra = EaseOutBounce(state_timer, 0, dropdist, anim_t);
yextra = clamp(yextra, 0, dropdist);

image_xscale = EaseOutElastic(state_timer, 1, 1 + xscale_animextra, anim_t);
image_xscale = clamp(image_xscale, 1, 1+xscale_animextra);



if canDrop && (state_timer>=18)
{
    canDrop = false;
    audio_play_sound(sndBoxDrop, 2, false);
}

if (state_timer >= (anim_t+10))
{
    state_switch("unlock"); 
}
