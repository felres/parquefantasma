/// make_color_random()
//
//  Returns a random color.
//
/// GMLscripts.com/license
{
    return random(c_white);
}
