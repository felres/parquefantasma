/// scrCopLooking
if state_new
{
    navangle = angle_normalize(navangle-90);
}

/// player detection
var col = collision_fov(x, y, objPlayer, sense, navangle, fov_angle, true);
if col && !collision_line(x, y, col.x, col.y, objCaster, true, true)
{
    player_instance = col;
    state_switch("detected");
}

if state_timer == floor(lookingTime)
{
    navangle = angle_normalize(navangle+180);
}
else if state_timer >= lookingTime*2
{    
    navangle = last_redirector.image_angle;
    state_switch("walking");
}
